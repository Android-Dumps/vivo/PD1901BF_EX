#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/bootdevice/by-name/recovery:67108864:5cb14b9a6ade010ec16558549e0aaa676b3c969d; then
  applypatch  EMMC:/dev/block/platform/bootdevice/by-name/boot:67108864:40610be2d43639d6c9664edc4887b6276e98e313 EMMC:/dev/block/platform/bootdevice/by-name/recovery 5cb14b9a6ade010ec16558549e0aaa676b3c969d 67108864 40610be2d43639d6c9664edc4887b6276e98e313:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
